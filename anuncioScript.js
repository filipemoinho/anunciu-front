//traz para a pagina as informacoes de token e nome
let codeAutorization = localStorage.getItem("wrCodeAutorization");
let nomeUsuarioLogin = localStorage.getItem("wrNomeUsuarioLogin");
let cpfUsuarioLogin = localStorage.getItem("wrCpfUsuarioLogin");
let descricaoReserva = document.querySelector("#descricaoReserva");
let statusAnuncio
let cpfAnuncio

document.querySelector("#btnConfirmarAnuncio").style.display="none";

let dataHora;
dataHora = new Date();
dataHora = dataHora.getUTCFullYear() + '-' +
        ('00' + (dataHora.getUTCMonth() + 1)).slice(-2) + '-' +
        ('00' + dataHora.getUTCDate()).slice(-2) 

let dataString = dataHora.toString();

if (nomeUsuarioLogin == null) {
    window.open("index.html","_self");
}

document.getElementById("anuncioNome").innerHTML = "Olá " + nomeUsuarioLogin;

let idAnuncio = getParameterByName("id");

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

carregarAnuncio();

function validaStatus(status, cpf){

  if (status != "Ativo"){
    let resp = document.getElementById("respostaAnuncio");
    if (status != "Vendido"){
      resp.innerHTML = "Anuncio já Reservado";
    } else {
      resp.innerHTML = "Anuncio já Vendido";
    }
    document.querySelector("#btnReservarAnuncio").style.display="none";
    document.querySelector("#descricaoReserva").style.display="none";

    if (cpf != cpfUsuarioLogin){
      document.querySelector("#btnCancelarReservarAnuncio").style.display="none";
    } else {
      if (status != "Reservado"){
        document.querySelector("#btnCancelarReservarAnuncio").style.display="none";
      } else {
        document.querySelector("#btnConfirmarAnuncio").style.display="block";
      }
    }

  } else {
    if (cpf == cpfUsuarioLogin){
      document.querySelector("#btnReservarAnuncio").style.display="none";
      document.querySelector("#descricaoReserva").style.display="none";
    }
    document.querySelector("#btnCancelarReservarAnuncio").style.display="none";
  }

}

function carregarAnuncio(){
    fetch('http://165.227.15.72:8080/anuncio/' + idAnuncio , {
      method: 'GET',
      headers: {
        'Authorization': codeAutorization
      }
    }).then(resposta => {
      return resposta.json();
    }).then(dados => {
      anuncio = dados;
  
      let lista = document.querySelector('#anuncioLista');

         let newAnuncio = document.createElement("a");
         let newLi = document.createElement("li");
         
         let date_new = new Date(anuncio.dataCadastro);
         let data_corrigida = Intl.DateTimeFormat('pt-BR').format(date_new);
        
         newAnuncio.innerHTML = `Titulo: ${anuncio.titulo} | Data Cadastro: ${data_corrigida} <br>
          Descrição: ${anuncio.descricao} <br><br>
           Valor: R$ ${anuncio.valor}`;

           validaStatus(anuncio.status,anuncio.usuario.cpf);

         newLi.appendChild(newAnuncio);
  
         lista.appendChild(newLi);
    });
  }

  function reservarAnuncio (){
    var dadosJson = '{"anuncio": {"id":' + idAnuncio + '}, '
    + '"dataReserva": "' + dataString + '", "descricao": "' + descricaoReserva.value + '"}'

    let dados = {
        anuncio: idAnuncio,
        dataReserva: dataHora,
        descricao: descricaoReserva.value
        
    };

    fetch("http://165.227.15.72:8080/reserva/cadastrar", {
        method: "POST",
        // body: JSON.stringify(text),
        body: dadosJson,
        headers: {
            'Content-type': 'application/json',
            'Authorization': codeAutorization
        }
    }).then(resposta => {
        if(resposta.status == 403){
            //
        }

        return resposta.json();
    }).then(dados => {

      let resp = document.getElementById("respostaAnuncio");
      resp.innerHTML = "Anuncio Reservado";
      document.querySelector("#btnReservarAnuncio").style.display="none";
      document.querySelector("#descricaoReserva").style.display="none";

    });
}

function cancelarReservarAnuncio (){
  var dadosJson = '{"id":' + idAnuncio + '}}'

  fetch("http://165.227.15.72:8080/anuncio/cancelarReserva", {
      method: "POST",
      body: dadosJson,
      headers: {
          'Content-type': 'application/json',
          'Authorization': codeAutorization
      }
  }).then(resposta => {
      if(resposta.status == 403){
          //
      }

      return resposta.json();
  }).then(dados => {
    let resp = document.getElementById("respostaAnuncio");
    resp.innerHTML = "Reserva cancelada!";
    document.querySelector("#btnConfirmarAnuncio").style.display="none";
    document.querySelector("#btnCancelarReservarAnuncio").style.display="none";
  });
}

function confirmarReservarAnuncio (){
  var dadosJson = '{"id":' + idAnuncio + '}}'

  fetch("http://165.227.15.72:8080/anuncio/confirmarReserva", {
      method: "POST",
      body: dadosJson,
      headers: {
          'Content-type': 'application/json',
          'Authorization': codeAutorization
      }
  }).then(resposta => {
      if(resposta.status == 403){
          //
      }

      return resposta.json();
  }).then(dados => {
    let resp = document.getElementById("respostaAnuncio");
    resp.innerHTML = "Reserva confirmada!";
    document.querySelector("#btnConfirmarAnuncio").style.display="none";
    document.querySelector("#btnCancelarReservarAnuncio").style.display="none";
  });
}

  let botaoReservar = document.querySelector("#btnReservarAnuncio");
  botaoReservar.onclick = function(){
    reservarAnuncio();
    // window.open("anuncio.html?id=" + idAnuncio ,"_self");
  }

  let btnCancelarReservarAnuncio = document.querySelector("#btnCancelarReservarAnuncio");
  btnCancelarReservarAnuncio.onclick = function(){
    cancelarReservarAnuncio();
    // window.open("anuncio.html?id=" + idAnuncio ,"_self");
  }


  let botaoVoltar = document.querySelector("#btnVoltar");
  botaoVoltar.onclick = function(){
    window.open("anuncios.html","_self");
  }

  let botaoDeslogar = document.querySelector("#btnDeslogar");
  botaoDeslogar.onclick = function(){
    localStorage.clear("wrCodeAutorization");
    window.open("index.html","_self");
  }

  let btnSetting = document.querySelector("#btnSetting");
  btnSetting.onclick = function(){
    window.open("setting.html","_self");
  }

  let btnConfirmarAnuncio = document.querySelector("#btnConfirmarAnuncio");
  btnConfirmarAnuncio.onclick = function(){
    confirmarReservarAnuncio();
    // window.open("anuncio.html?id=" + idAnuncio ,"_self");
  }
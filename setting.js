//traz para a pagina as informacoes de token e nome
let codeAutorization = localStorage.getItem("wrCodeAutorization");
let nomeUsuarioLogin = localStorage.getItem("wrNomeUsuarioLogin");

let nomeUsuario = document.querySelector("#nomeUsuario");
let cpfUsuario = document.querySelector("#cpfUsuario");
let funcionalUsuario = document.querySelector("#funcionalUsuario");
let emailUsuario = document.querySelector("#emailUsuario");
let cepUsuario = document.querySelector("#cepUsuario");
let enderecoUsuario = document.querySelector("#enderecoUsuario");
let senhaUsuario = document.querySelector("#senhaUsuario");



let dataHora = new Date();
dataHora = dataHora.getUTCFullYear() + '-' +
        ('00' + (dataHora.getUTCMonth() + 1)).slice(-2) + '-' +
        ('00' + dataHora.getUTCDate()).slice(-2) 

let dataString = dataHora.toString();

if (nomeUsuarioLogin == null) {
    window.open("index.html","_self");
}

document.getElementById("anuncioNome").innerHTML = "Olá " + nomeUsuarioLogin;


carregarDadosUsuario();

function carregarDadosUsuario(){
    fetch('http://165.227.15.72:8080/usuario/consulta', {
      method: 'GET',
      headers: {
        'Authorization': codeAutorization
      }
    }).then(resposta => {
      return resposta.json();
    }).then(dados => {
      usuario = dados;

        nomeUsuario.value = (usuario.nome);
        cpfUsuario.value = (usuario.cpf);
        funcionalUsuario.value = (usuario.funcional);
        emailUsuario.value = (usuario.email);
        cepUsuario.value = (usuario.cep);
        enderecoUsuario.value = (usuario.endereco);

    });
  }

  function atualizarUsuario (){
    let dados = {
        nome: nomeUsuario.value,
        funcional: funcionalUsuario.value,
        cpf: cpfUsuario.value,
        email: emailUsuario.value,
        senha: senhaUsuario.value,
        cep: cepUsuario.value,
        endereco: enderecoUsuario.value
    };

    fetch("http://165.227.15.72:8080/usuario/alteracao", {
        method: "POST",
        body: JSON.stringify(dados),
        headers: {
            'Content-type': 'application/json',
            'Authorization': codeAutorization
        }
    }).then(resposta => {
        if(resposta.status == 403){
            //
        }

        return resposta.json();
    }).then(dados => {
        let resp = document.getElementById("respostaSetting");
        resp.innerHTML = "Dados Atualizados!"
    });
}

  let btnAtualizarCadastro = document.querySelector("#btnAtualizarCadastro");
  btnAtualizarCadastro.onclick = function(){
    atualizarUsuario();
  }


  let botaoVoltar = document.querySelector("#btnVoltar");
  botaoVoltar.onclick = function(){
    window.open("anuncios.html","_self");
  }

  let botaoDeslogar = document.querySelector("#btnDeslogar");
  botaoDeslogar.onclick = function(){
    localStorage.clear("wrCodeAutorization");
    window.open("index.html","_self");
  }

  let btnSetting = document.querySelector("#btnSetting");
  btnSetting.onclick = function(){
    window.open("setting.html","_self");
  }

  let btnSettingReserva = document.querySelector("#btnSettingReserva");
  btnSettingReserva.onclick = function(){
    window.open("settingReservas.html","_self");
  }

  let btnSettingAnuncio = document.querySelector("#btnSettingAnuncio");
  btnSettingAnuncio.onclick = function(){
    window.open("settingAnuncios.html","_self");
  }
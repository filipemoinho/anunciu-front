//traz para a pagina as informacoes de token e nome
let codeAutorization = localStorage.getItem("wrCodeAutorization");
let nomeUsuarioLogin = localStorage.getItem("wrNomeUsuarioLogin");

let categoriaAnuncio = document.querySelector("#categoriaAnuncio");
let tituloAnuncio = document.querySelector("#tituloAnuncio");
let descricaoAnuncio = document.querySelector("#descricaoAnuncio");
let valorAnuncio = document.querySelector("#valorAnuncio");


let dataHora = new Date();
dataHora = dataHora.getUTCFullYear() + '-' +
        ('00' + (dataHora.getUTCMonth() + 1)).slice(-2) + '-' +
        ('00' + dataHora.getUTCDate()).slice(-2) 

let dataString = dataHora.toString();

if (nomeUsuarioLogin == null) {
    window.open("index.html","_self");
}

document.getElementById("anuncioNome").innerHTML = "Olá " + nomeUsuarioLogin;

  function cadastrarAnuncio (){
    let dados = {
        categoria: categoriaAnuncio.value,
        titulo: tituloAnuncio.value,
        descricao: descricaoAnuncio.value,
        valor: valorAnuncio.value,
        dataCadastro: dataString,
        status: "Ativo"
    };

    fetch("http://165.227.15.72:8080/anuncio/cadastrar", {
        method: "POST",
        body: JSON.stringify(dados),
        headers: {
            'Content-type': 'application/json',
            'Authorization': codeAutorization
        }
    }).then(resposta => {
        if(resposta.status == 403){
            //
            let resp = document.getElementById("respostaCadastrar");
            resp.innerHTML = "Ocorreu algum problema tecnico";
        }

        return resposta.json();
    }).then(dados => {
        console.log(dados);
        let resp = document.getElementById("respostaCadastrar");
        resp.innerHTML = "Cadastro realizado com sucesso!";
    });
}

  let btnCadastrarAnuncio = document.querySelector("#btnCadastrarAnuncio");
  btnCadastrarAnuncio.onclick = function(){
    cadastrarAnuncio();
    // window.open("anuncios.html","_self");
  }


  let botaoVoltar = document.querySelector("#btnVoltar");
  botaoVoltar.onclick = function(){
    window.open("anuncios.html","_self");
  }

  let botaoDeslogar = document.querySelector("#btnDeslogar");
  botaoDeslogar.onclick = function(){
    localStorage.clear("wrCodeAutorization");
    window.open("index.html","_self");
  }

  let btnSetting = document.querySelector("#btnSetting");
  btnSetting.onclick = function(){
    window.open("setting.html","_self");
  }
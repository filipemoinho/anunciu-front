//LEITURA DOS CAMPOS
//------------------
let cadastroNome = document.querySelector("#cadastroNome");
let cadastroFuncional = document.querySelector("#cadastroFuncional");
let cadastroCpf = document.querySelector("#cadastroCpf");
let cadastroEmail = document.querySelector("#cadastroEmail");
let cadastroSenha = document.querySelector("#cadastroSenha");
let cadastroCep = document.querySelector("#cadastroCep");
let cadastroEndereco = document.querySelector("#cadastroEndereco");


//LEITURA DOS CAMPOS
//------------------
let loginCpf = document.querySelector("#loginCpf");
let loginSenha = document.querySelector("#loginSenha");

var codeAutorization = "";
var nomeUsuarioLogin = "";

//funçao que será executada quando for precionado o botão Cadastrar
let botaoNovoUsuario = document.querySelector("#btnNovoUsuario");
botaoNovoUsuario.onclick = function(){
    document.getElementById("cadastro").style.display = "block";
    document.getElementById("btnNovoUsuario").style.display = "none";
    document.getElementById("btnLogar").style.display = "none";
}

//funçao que será executada quando for precionado o botão Cadastrar
let botaoCancelar = document.querySelector("#btnCancelar");
botaoCancelar.onclick = function(){
    escondeNovoUsuario();
}

//funçao que será executada quando for pressionado o botão Cancelar
let botaoCadastrar = document.querySelector("#btnCadastrar");
botaoCadastrar.onclick = cadastrarUsuario;

function escondeNovoUsuario (){
    document.getElementById("cadastro").style.display = "none";
    document.getElementById("btnNovoUsuario").style.display = "block";
    document.getElementById("btnLogar").style.display = "block";
}


function cadastrarUsuario (){
    let dados = {
        nome: cadastroNome.value,
        funcional: cadastroFuncional.value,
        cpf: cadastroCpf.value,
        email: cadastroEmail.value,
        senha: cadastroSenha.value,
        cep: cadastroCep.value,
        endereco: cadastroEndereco.value
    };

    fetch("http://165.227.15.72:8080/usuario", {
        method: "POST",
        body: JSON.stringify(dados),
        headers: {
            'Content-type': 'application/json'
        }
    }).then(resposta => {
        if(resposta.status == 403){
            //
        }

        return resposta.json();
    }).then(dados => {
        console.log(dados);
        escondeNovoUsuario();
    });
}

//funçao que será executada quando for pressionado o botão Cancelar
let botaoLogar = document.querySelector("#btnLogar");
botaoLogar.onclick = logarUsuario;

function logarUsuario (){
    let abrir=false;
    let dados = {
        cpf: loginCpf.value,
        senha: loginSenha.value
    };

    fetch("http://165.227.15.72:8080/usuario/login", {
        method: "POST",
        body: JSON.stringify(dados),
        headers: {
            'Content-type': 'application/json'
        }
    }).then(resposta => {
        if(resposta.status !== 200){
            let resp = document.getElementById("respostaLogar");
            resp.innerHTML = "Senha ou Usuário inválido";
            return;
        } else{
            abrir = true;
            codeAutorization = resposta.headers.get("Authorization");
            console.log ("Token " + codeAutorization);
            return resposta.json();
        }
    }).then(dados => {
        if (abrir == true){
            nomeUsuarioLogin =  dados.nome;
            CpfUsuarioLogin = dados.cpf;
            console.log ("Nome do usuario " + nomeUsuarioLogin);
            localStorage.setItem("wrCodeAutorization", codeAutorization);
            localStorage.setItem("wrNomeUsuarioLogin", nomeUsuarioLogin);
            localStorage.setItem("wrCpfUsuarioLogin", CpfUsuarioLogin);
            window.open("anuncios.html","_self");
        }
    });
}

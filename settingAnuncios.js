//traz para a pagina as informacoes de token e nome
let codeAutorization = localStorage.getItem("wrCodeAutorization");
let nomeUsuarioLogin = localStorage.getItem("wrNomeUsuarioLogin");

if (nomeUsuarioLogin == null) {
    window.open("index.html","_self");
}

document.getElementById("anuncioNome").innerHTML = "Olá " + nomeUsuarioLogin;

carregarAnuncios();

function carregarAnuncios(){
    fetch('http://165.227.15.72:8080/anuncios/usuario', {
      method: 'GET',
      headers: {
        'Authorization': codeAutorization
      }
    }).then(resposta => {
      return resposta.json();
    }).then(dados => {
      anuncios = dados;

      // console.log (anuncios);
  
      let lista = document.querySelector('#anuncioLista');
  
      for(let anuncio of anuncios){
        let newAnuncio = document.createElement("a");
        let newLi = document.createElement("li");
        newAnuncio.href = "anuncio.html?id=" + anuncio.id;
        newLi.id = `${anuncio.id}`;
        
        newAnuncio.innerHTML = `<div class="list-group-item"> ${anuncio.titulo} |valor: R$ ${anuncio.valor}</div>`;
        newLi.appendChild(newAnuncio);
  
        lista.appendChild(newLi);
      }
    });
  }

  let btnCadastrarAnuncio = document.querySelector("#btnCadastrarAnuncio");
  btnCadastrarAnuncio.onclick = function(){
    window.open("cadastrarAnuncio.html","_self");
  }

  let btnSetting = document.querySelector("#btnSetting");
  btnSetting.onclick = function(){
    window.open("setting.html","_self");
  }

  let botaoDeslogar = document.querySelector("#btnDeslogar");
  botaoDeslogar.onclick = function(){
    localStorage.clear("wrCodeAutorization");
    window.open("index.html","_self");
  }

  let botaoVoltar = document.querySelector("#btnVoltar");
  botaoVoltar.onclick = function(){
    window.open("setting.html","_self");
  }
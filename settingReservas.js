//traz para a pagina as informacoes de token e nome
let codeAutorization = localStorage.getItem("wrCodeAutorization");
let nomeUsuarioLogin = localStorage.getItem("wrNomeUsuarioLogin");

if (nomeUsuarioLogin == null) {
    window.open("index.html","_self");
}

document.getElementById("anuncioNome").innerHTML = "Olá " + nomeUsuarioLogin;

carregarReservas();

function carregarReservas(){
    fetch('http://165.227.15.72:8080/reservas/usuario', {
      method: 'GET',
      headers: {
        'Authorization': codeAutorization
      }
    }).then(resposta => {
      return resposta.json();
    }).then(dados => {
      reservas = dados;
  
      let lista = document.querySelector('#reservaLista');
  
      for(let reserva of reservas){
        let newReserva = document.createElement("a");
        let newLi = document.createElement("li");
        
        let status = "";

        console.log(reserva.status)
        if (reserva.status == false) {
          status = "Reserva Cancelada"
        } else {
          status = "Reserva Ativa"
        }

        newReserva.innerHTML = `<div class="list-group-item"> ID: ${reserva.id} | Anuncio: ${reserva.anuncio.titulo} | Status: ${status} <br>
        | Vendedor: ${reserva.anuncio.usuario.nome} | Funcional: ${reserva.anuncio.usuario.funcional} . O procure no portal pessoas! <br></div>`;
        newLi.appendChild(newReserva);
  
        lista.appendChild(newLi);
      }
    });
  }

  let btnSetting = document.querySelector("#btnSettings");
  btnSetting.onclick = function(){
    window.open("setting.html","_self");
  }

  let botaoDeslogar = document.querySelector("#btnDeslogar");
  botaoDeslogar.onclick = function(){
    localStorage.clear("wrCodeAutorization");
    window.open("index.html","_self");
  }

  let botaoVoltar = document.querySelector("#btnVoltar");
  botaoVoltar.onclick = function(){
    window.open("setting.html","_self");
  }